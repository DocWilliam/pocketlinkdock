<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use User;

class Privacy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(User::where('handle',$request->handle)->first() == null)
            abort(401);

        try {
            $header = $request->header('Authorization');
    
            $token = User::where('handle',$request->handle)->first()->token;
            if ((str_contains($header,$token) || $header == $token) && $token != '') {
                return $next($request);
            }
            else
                abort(401);

        } catch (\Throwable $th) {
            abort(401);
        }

    }
}
