<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;

class Secret
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        try {
            $token = $request->route('token');
            JWT::decode($token,'MYirP3F82yjEGxHBZB2N3rK82cFyGE34chaK35vjly2yjQOC4cNZUDGtAd9YwPtq', array('HS256'));
            return $next($request);

        } catch (\Throwable $th) {
            return response($th);
        }            
    }
}
