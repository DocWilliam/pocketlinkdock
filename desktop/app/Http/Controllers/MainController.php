<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pocket;
use App\Models\Link;
use Illuminate\Support\Facades\Redirect;

class MainController extends Controller
{
    public function user(Request $request){
        return User::where('handle',$request->handle)->first();
    }

    public function pockets(Request $request){
        return User::find($request->id)->pockets()->get();
    }

    public function pocketLinks(Request $request){
        return Pocket::find($request->id)->links()->get();
    }

    public function userLinks(Request $request){
        return User::find($request->id)->pockets()->get();
    }

    public function handleExists(Request $request){
        $user = new User;

        return array('exists'=>$user->handleExists($request->handle));
    }

    public function redirectLogin(Request $request){
        return Redirect::away('http://localhost:3000/login');
    }

    public function redirectDashboard(Request $request){
        return Redirect::away('http://localhost:3000/es/dashboard');
    }

    public function confirmSession(Request $request){
        
        $user = User::where('handle',$request->handle)->first();

        $user->token = $request->cookie('pocketlink_session');

        return $user->save();
    }

    public function tokenCreate(Request $request){

        try {
            $token = $request->user()->createToken('api_token');
    
            $user = User::find($request->user()->id);
    
            $user->token = $token->plainTextToken;
    
            $user->save();
    
            return ['token' => $token->plainTextToken];
        } catch (\Throwable $th) {
            abort(401);
        }

    }

    public function isLogged(Request $request, $handle){

        $user = User::where('handle',$handle)->where('token','!=','')->first();

        if($user){
            return true;
        }
        else
            abort(401);
    }

    public function addPocket(Request $request){

        $pocket = new Pocket;

        $pocket->name = $request->name;
        $pocket->header = $request->header;
        $pocket->description = $request->description;
        $pocket->pocket_email = $request->pocketEmail;
        $pocket->location = $request->location;
        $pocket->type = $request->type;
        $pocket->main = $request->main;
        $pocket->user_id = $request->userId;

        try {
            $pocket->save();

            return response()->created();
            
        } catch (\Throwable $th) {
            abort('wrong data',422); 
        }
    }

    public function addLink(Request $request){

        $link = new Link;

        $link->name = $request->name;
        $link->url = $request->url;
        $link->application_id = $request->applicationId;
        $link->pocket_id = $request->pocketId;

        try {
            $link->save();

            return response()->created();
            
        } catch (\Throwable $th) {
            return abort(422);
        }
    }

    public function moveLink(Request $request){

        $link = Link::find($request->linkId);

        $link->pocket_id = $request->pocketId;

        $link->save();

    }

    public function removePocket(Request $request){

        try {

            Link::where('pocket_id', $request->id)->delete();

            Pocket::destroy($request->id);
            return response()->deleted();

        } catch (\Throwable $th) {
            return abort(422);
        }        

    }

    public function removeLink(Request $request){

        try {
            Link::destroy($request->id);
            return response()->deleted();
            
        } catch (\Throwable $th) {
            return abort(422);
        }

    }
}
