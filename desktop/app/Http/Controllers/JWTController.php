<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Firebase\JWT\JWT;

class JWTController extends Controller
{
    public function token(Request $request){

        $time = time();
        $key = 'MYirP3F82yjEGxHBZB2N3rK82cFyGE34chaK35vjly2yjQOC4cNZUDGtAd9YwPtq';
    
        $token = array(
            'iat' => $time, // Tiempo que inició el token
            'exp' => $time + (60), // Tiempo que expirará el token (+1 hora)
            'data' => [ // información del usuario
                '_token' => csrf_token(),
            ]
        );
    
        $jwt = JWT::encode($token, $key);
    
        return array('csrf'=>$jwt);
    }
}
