<?php

namespace App\Actions\Fortify;

use User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255',Rule::unique(User::class),],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
        ])->validate();

        return User::create([
            'name' => $input['name'],
            'handle' => $input['name'],
            'email' => $input['email'],
            'location' => 1,
            'license' => 1,
            'referral' => $this->createHash($input['name']),
            'password' => Hash::make($input['password']),

        ]);
    }

    private function createHash($inputString){
        $coded = '';
                
        $inputString = strtr($inputString,[' '=>'']);        
        $inputString = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', substr($inputString,0,7));
        $inputString = strtr($inputString,['?'=>'']);
        

        for($i = 0; $i < strlen($inputString); $i++){
    
            if($i % 2 == 0)
                $coded .= ord($inputString[$i]);
            else{
                if($i % 3 != 0)
                    $coded .= $inputString[$i];
                else
                    $coded .= strtoupper($inputString[$i]);
            }
        
        }

        return $coded;
    }
}
