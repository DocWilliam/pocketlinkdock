<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JWTController;
use App\Http\Controllers\MainController;
use Firebase\JWT\JWT;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// middleware(['secret','cors'])->

// Route::middleware(['secret','cors'])->get('/token/{token}',[JWTController::class,'token'])->name('token');

Route::post('/uploadimage',[MainController::class,'uploadImage'])->name('uploadimage');

Route::post('/uploadlink',[MainController::class,'uploadLink'])->name('uploadlink');

Route::middleware('auth')->post('/token/create', [MainController::class,'tokenCreate'])->name('tokencreate');

Route::get('/redirectLogin',[MainController::class,'redirectLogin'])->name('redirectlogin');

Route::get('logged/{handle}',[MainController::class,'isLogged']);
