<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['web','privacy'])->post('/user',[MainController::class, 'user'])->name('user');

Route::middleware(['web'])->post('/confirmSession',[MainController::class, 'confirmSession'])->name('confirmession');

Route::post('/pockets',[MainController::class, 'pockets'])->name('pockets');

Route::post('/pocketlinks',[MainController::class, 'pocketLinks'])->name('pocketlinks');

Route::post('/userlinks',[MainController::class, 'userLinks'])->name('userlinks');

Route::post('/handleExists',[MainController::class, 'handleExists'])->name('handleexists');

Route::post('/addPocket',[MainController::class, 'addPocket'])->name('addpocket');

Route::post('/addLink',[MainController::class, 'addLink'])->name('addlinks');

Route::post('/moveLink',[MainController::class, 'moveLink'])->name('movelink');

Route::post('/removePocket',[MainController::class, 'removePocket'])->name('removepocket');

Route::post('/removeLink',[MainController::class, 'removeLink'])->name('removelink');